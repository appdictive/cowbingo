#!/usr/bin/env python
"""\
CowBingo.py streams a series of gcode messages to a 3D printer. The code will move the extruder randomly in the 
Y/X axis within 10x9 cell grid. Each time the extruder reaches a cell it will decide to either extrude some fillament
or wait for a few seconds and move on to the next position. 
The code will only extrude on a cell once. 
  
"""
 
import serial
import time
import random
import atexit


outcomes = range(1, 91)

square_width = 17.5
x_offset = 14
y_offset = 22

sleep_time_min = 1
sleep_time_max = 4

poo_probability = 0.25

port = '/dev/ttyUSB0'
baud = 250000

    
def getGcodePosition(doRemove):
    global __max_x
    global __max_y
    global outcomes
    global square_width

    randomPos = random.randrange(len(outcomes))
    if doRemove:
        number =outcomes.pop(randomPos)
        print "removing from list"
    else:
        number = outcomes[randomPos]
    print "number= " + str(number)
    x = 10 - number%10 #10 is due to our axis being inverted
    y= number/10 
    if x== 10:
        x = 0
        y -= 1;
    
    xPos = x_offset + x * square_width
    yPos = y_offset + y * square_width

    return "G1 X"+str(xPos) +" Y" + str(yPos)
    
    
def moveAtRandom():
    while True:
        global sleep_time_max
        global sleep_time_min

        if len(outcomes) == 0:
            return


        if random.randrange(100) < (poo_probability * 100):
            __do_poo = True
        else:
            __do_poo = False
            
        gcodePos = getGcodePosition(__do_poo)
        print "sending: " + gcodePos
        sendGcodeAndAwaitResponse(gcodePos)
        sendGcodeAndAwaitResponse("M400")

        time.sleep(random.randrange(sleep_time_min, sleep_time_max))
        if __do_poo:
            sendGcodeAndAwaitResponse("G1 F1000")
            sendGcodeAndAwaitResponse("G1 Z1.8")
            sendGcodeAndAwaitResponse("G1 F20")
            sendGcodeAndAwaitResponse("G1 F20 Z04 E30")
            print "TAKING A DUMP"

            sendGcodeAndAwaitResponse("G1 F1000")
            sendGcodeAndAwaitResponse("G1 Z10.0")
            sendGcodeAndAwaitResponse("M400")
            sendGcodeAndAwaitResponse("G1 F4000")


def sendGcodeAndAwaitResponse(gcode):
    s.write(gcode + '\n')  #
    print "reading: " + s.readline()

# Open reprap serial port
s = serial.Serial(port, baud)

s.write("\r\n\r\n")
time.sleep(2)   # Wait for reprap to initialize
s.flushInput()  # Flush startup text in serial input

f = open('header.gcode','r');

# initialize by sending content in header
for line in f:
    line = line.strip() # Strip all EOL characters for streaming
    print 'Sending: ' + line,
    sendGcodeAndAwaitResponse(line)

s.write("M400" + '\n') # wait for positions to be reached

status = ""
while  status != "ok": #while not ready (just getting temp updates), wait
    status = s.readline().strip()
    print "awaiting start : " + status

print "reading: " + s.readline() 

def onexit():
    # Close file and serial port
    f.close()
    s.close()

atexit.register(onexit)

moveAtRandom()
