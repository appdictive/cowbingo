M109 S210.000000
;Sliced at: Sat 30-04-2016 10:18:43
;Basic settings: Layer height: 0.2 Walls: 0.7 Fill: 30
;Print time: 4 minutes
;M190 S60 ;Uncomment to add your own bed temperature line
;M109 S240 ;Uncomment to add your own temperature line
G21        ;metric values
G90        ;absolute positioning
M83        ;set extruder to absolute mode
M107       ;start with the fan off
M117 homing x y
G28
G1 Z10.0 F1000 ;move the platform down 15mm
G92 E0                  ;zero the extruded length again
G1 F4000
;Put printing message on LCD screen
M117 Printing...

;LAYER:0
M107
G1 F4000 X0 Y0  ;move X/Y to min endstops
M400